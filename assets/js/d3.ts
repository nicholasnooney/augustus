import * as d3_module from 'js/modules/d3/d3';

declare global {
  var d3: any;
}
globalThis.d3 = d3_module;
