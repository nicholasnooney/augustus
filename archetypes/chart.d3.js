// chart.d3.js - a template for a D3.js chart.
//
// Use this template to create a self-contained D3 chart. Replace '#filename'
// with the name of the file, excluding the '.d3.js' extension, and add the
// chart to a markdown file with the d3_graphic shortcode.

const container = d3.select("#filename");
const margin = { top: 0, right: 0, bottom: 0, left: 0 };
const height = 75
const width = 100

const svg = container.append("svg")
  .attr("viewBox", [0, 0, width, height]);
