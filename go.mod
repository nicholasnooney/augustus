module gitlab.com/nicholasnooney/augustus

go 1.15

require (
	gitlab.com/nicholasnooney/hugolibs/d3 v0.0.0-20210911193741-99387b01ba3c // indirect
	gitlab.com/nicholasnooney/hugolibs/feather-icons v0.0.0-20210911193741-99387b01ba3c // indirect
)
