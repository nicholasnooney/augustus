const defaultTheme = require('tailwindcss/defaultTheme');

const themeDir = __dirname;

module.exports = {
  purge: [
    themeDir + '/layouts/**/*.html',
  ],
  darkMode: 'media', // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        carrara: { DEFAULT: '#F1F2F3', },
        scooter: { DEFAULT: '#11D1D1', },
        shakespeare: { DEFAULT: '#75BAD2', },
        noonblue: { DEFAULT: '#0080D8', },
        calypso: { DEFAULT: '#396795', },
        valhalla: { DEFAULT: '#121B32', },
      },
      fontFamily: {
        sans: ['Noto Sans', ...defaultTheme.fontFamily.sans],
        serif: ['Noto Serif', ...defaultTheme.fontFamily.serif],
        mono: ['Roboto Mono', ...defaultTheme.fontFamily.mono],
      },
      typography: (theme) => ({
        DEFAULT: {
          css: {
            color: theme('colors.valhalla.DEFAULT'),
            a: {
              color: theme('colors.valhalla.DEFAULT'),
              code: { color: theme('colors.valhalla.DEFAULT') },
            },
            blockquote: {
              borderLeftColor: theme('colors.valhalla.DEFAULT'),
              color: theme('colors.valhalla.DEFAULT'),
            },
            code: { color: theme('colors.valhalla.DEFAULT'), },
            figure: { figcaption: { color: theme('colors.valhalla.DEFAULT') } },
            h1: { color: theme('colors.valhalla.DEFAULT') },
            h2: { color: theme('colors.valhalla.DEFAULT') },
            h3: { color: theme('colors.valhalla.DEFAULT') },
            h4: { color: theme('colors.valhalla.DEFAULT') },
            hr: { borderColor: theme('colors.valhalla.DEFAULT') },
            ol: { li: { '&:before': { color: theme('colors.valhalla.DEFAULT') } } },
            strong: { color: theme('colors.valhalla.DEFAULT') },
            table: {
              pre: {
                margin: 0,
                fontSize: '1em',
              }
            },
            thead: {
              borderBottomColor: theme('colors.valhalla.DEFAULT'),
              color: theme('colors.valhalla.DEFAULT'),
            },
            tbody: {
              tr: { borderBottomColor: theme('colors.valhalla.DEFAULT'), },
              td: {
                '&:first-child': { pre: { paddingRight: 0, }, },
                '&:last-child': { pre: { paddingLeft: 0, }, },
              },
            },
            ul: { li: { '&:before': { backgroundColor: theme('colors.valhalla.DEFAULT') } } },
          }
        },
        'dark': {
          css: {
            color: theme('colors.carrara.DEFAULT'),
            a: {
              color: theme('colors.carrara.DEFAULT'),
              code: { color: theme('colors.carrara.DEFAULT') },
            },
            blockquote: {
              borderLeftColor: theme('colors.carrara.DEFAULT'),
              color: theme('colors.carrara.DEFAULT'),
            },
            code: { color: '#d8dee9', background: '#2e3440', },
            figure: { figcaption: { color: theme('colors.carrara.DEFAULT') } },
            h1: { color: theme('colors.carrara.DEFAULT') },
            h2: { color: theme('colors.carrara.DEFAULT') },
            h3: { color: theme('colors.carrara.DEFAULT') },
            h4: { color: theme('colors.carrara.DEFAULT') },
            hr: { borderColor: theme('colors.carrara.DEFAULT') },
            ol: { li: { '&:before': { color: theme('colors.carrara.DEFAULT') } } },
            strong: { color: theme('colors.carrara.DEFAULT') },
            table: {
              pre: {
                margin: 0,
                fontSize: '1em',
              }
            },
            thead: {
              borderBottomColor: theme('colors.carrara.DEFAULT'),
              color: theme('colors.carrara.DEFAULT'),
            },
            tbody: {
              tr: { borderBottomColor: theme('colors.carrara.DEFAULT'), },
              td: {
                '&:first-child': { pre: { paddingRight: 0, }, },
                '&:last-child': { pre: { paddingLeft: 0, }, },
              },
            },
            ul: { li: { '&:before': { backgroundColor: theme('colors.carrara.DEFAULT') } } },
          }
        }
      }),
    },
  },
  variants: {
    extend: {
      typography: ['dark'],
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
  ],
}
